# Nazca Tech & Consulting

## Prueba de Desarrollo: Class (Classes)

Pericia en manejo de objetos

## 01 - Bases

Crear una clase para procesar números que se llame `Numbers`.

esta clase debe cumplir con el siguiente funcionamiento

``` PHP
$num = new Numbers();

echo $num->sum( 3, 5) // 3 + 5 = 8

echo $num->diff( 2, 4) // 2 - 4 = -2

echo $num->format( 1262.25361154, 4) // 1,262.2536
echo $num->format( 1262.25361154) // 1,262.25

echo $num->money( 1262.25361154) // $ 1,262.25

```

## 02 - Métodos Estáticos

Convertir los métodos de la clase `Numbers` a llamadas estáticas para poder utilizarlos de la siguiente manera

``` PHP
echo Number::sum( 3, 5) // 3 + 5 = 8

echo Number::diff( 2, 4) // 2 - 4 = -2

echo Number::format( 1262.25361154, 4) // 1,262.2536
echo Number::format( 1262.25361154) // 1,262.25

echo Number::money( 1262.25361154) // $ 1,262.25
```

## 03 - Constantes

Crear una clase `NumbersConfig`.

dentro de esta clase se deben colocar tres constantes que deben seguir el standard

|constante          |tipo   |
|-------------------|-------|
|decimals           |Int    |
|decimal separator  |Str    |
|thousands separator|Str    |

esta clase define el comportamiento de las funciones `format` y `money` de la clase `Numbers` en forma que:

``` PHP
// decimals = 4
echo Number::format( 1262.25361154) // 1,262.2536

// decimals = 4
// decimal separator = "."
// thousands separator = " "
echo Number::format( 1262.25361154) // 1 262,2536
echo Number::money( 1262.25361154) // $ 1 262,2536
```

## 04 - Herencia

Se debe crear una clase llamada `Operations` esta debe extender de `Numbers`

las operaciones de calculo `multi` y `div` deben existir en esta clase

el comportamiento deseado es:

``` PHP
echo Operations::sum( 3, 5) // 3 + 5 = 8
echo Operations::diff( 2, 4) // 2 - 4 = -2
echo Operations::multi( 2, 4) // 2 x 4 = 8
echo Operations::div( 6, 2) // 6 / 2 = 3

echo Operations::format( 1262.25361154, 4) // 1,262.2536
echo Operations::format( 1262.25361154) // 1,262.25

echo Operations::money( 1262.25361154) // $ 1,262.25

// decimals = 4
echo Operations::format( 1262.25361154) // 1,262.2536

// decimals = 4
// decimal separator = "."
// thousands separator = " "
echo Operations::format( 1262.25361154) // 1 262,2536
echo Operations::money( 1262.25361154) // $ 1 262,2536
```
