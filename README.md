# Nazca Tech & Consulting

## Prueba de Desarrollo

Lista de pruebas para quilificar a nuevos prospectos de personal.

## Normas

Para responder se debe crear una estructura de carpetas en el directorio `/Respuestas` alusivo al tema y prueba

```bash
> /Test/...
> /Respuestas/PHP/DataManipulation/01/
> /Respuestas/PHP/DataManipulation/02/
> /Respuestas/PHP/Arrays/01/
> /Respuestas/PHP/Arrays/02/
> /Respuestas/PHP/Arrays/03/
> ...
```

dentro de cada una de estas capetas se deben tener su respectivo `index.php` que debe mostrar las soluciones

> Se recomienda trabajar siguiendo los estándar PSR, en este caso PSR-1,PSR-2 y PSR-4
>
> [PHP-FIG PSR : PHP Standards Recommendations](https://www.php-fig.org/psr/)

## Pruebas

- PHP

  - [Data Manipulation](./Test/PHP/01-Data.md)
  - [Arrays](./Test/PHP/02-Arrays.md)
  - [Class](./Test/PHP/03-Clases.md)
    - [Bases](./Test/PHP/03-Clases.md)
    - [Static methods](./Test/PHP/03-Clases.md)
    - [Constantes](./Test/PHP/03-Clases.md)
    - [Herencia](./Test/PHP/03-Clases.md)
  - [Composer](./Test/PHP/04-Composer.md)
  - [PSR4 Auto-loader](./Test/PHP/05-PSR4.md)
  - [Laravel](./Test/PHP/06-Laravel.md)

- JS
  - [jQuery](./Test/JS/01-JQuery-DataTables.md)
    - [DataTables\.net](./Test/JS/01-JQuery-DataTables.md)
