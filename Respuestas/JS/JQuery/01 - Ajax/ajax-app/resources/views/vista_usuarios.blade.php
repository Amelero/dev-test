<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tabla de Usuarios</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    </head>
    <body>
        <h1>Tabla de Usuarios</h1>
        <table id="users-table" border="1">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>City</th>
                    <th>Company</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script>
            $(document).ready(function () {
                $.ajax({
                    url: "https://jsonplaceholder.typicode.com/users",
                    dataType: "json",
                    success: function (data) {
                        var tableBody = $("#users-table tbody");

                        $.each(data, function (index, user) {
                            var row = $("<tr>");
                            row.append($("<td>").text(user.id));
                            row.append($("<td>").text(user.name));
                            row.append($("<td>").text(user.email));
                            row.append($("<td>").text(user.address.city));
                            row.append($("<td>").text(user.company.name));
                            tableBody.append(row);
                        });
                    }
                });
            });
        </script>
    </body>
</html>
