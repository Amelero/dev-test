<?php
    // Usando la función array_reduce calcular promedio de edades de $matrix
    $matrix = [
        ['user'=>'A', 'name'=>'a a', 'age'=>17],
        ['user'=>'B', 'name'=>'b b', 'age'=>22],
        ['user'=>'C', 'name'=>'c c', 'age'=>36],
        ['user'=>'D', 'name'=>'d d', 'age'=>42],
        ['user'=>'E', 'name'=>'e e', 'age'=>12],
    ];

    $promedio = array_reduce($matrix, function($carry, $item) {
        return $carry + $item['age'];
    }, 0);

    $promedio /= count($matrix); //promedio = suma/cantidad
    
    echo "El promedio de edades es: $promedio"."</br>";

    //Usando la función array_filter solo los mayores de 30 años $matrix
    $mayores = array_filter($matrix, function($item) {
        return $item['age'] > 30;
    });

    echo "Edades mayores a 30: </br>";
    print_r($mayores);

    // iterar dentro de un loop foreach para mostrar la data en una tabla;
    // primero armamos la tabla
    echo "<table>";
    echo "<tr><th>Usuario</th><th>Nombre</th><th>Edad</th></tr>";
    // recorremos matriz
    foreach($matrix as $i) {
        echo "<tr><td>{$i['user']}</td><td>{$i['name']}</td><td>{$i['age']}</td></tr>";
    }
    echo "</table>";
    
    //Usando la función array_map transformar cada indice dentro de $matrix para agregar la marca de mayor de edad
    $matrix_calc = array_map(function($matriz) {
        $matriz['adult'] = ($matriz['age'] >= 18); // Agregar clave "adult" con valor booleano
        $matriz['adult'] = $matriz['adult'] ? 'true' : 'false'; // Modificar valor de clave "adult" a "true" o "false" (tiene)
        return $matriz; // Devolver elemento modificado
    }, $matrix);
    
    print_r($matrix_calc);

    echo"</br></br>";

    //
    $json_string = json_encode($matrix_calc);
    echo "Matriz en formato JSON:  ".$json_string;
?>