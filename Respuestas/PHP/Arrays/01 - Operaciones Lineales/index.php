<?php
    $array = [1,3,2,3,4,5,6,7];
    //Usando la función array_reduce calcular el acumulado de los valores de $array
    $acumulado = array_reduce($array, function ($valor_acumulado, $elemento) {
        return $valor_acumulado + $elemento;
    }, 0);
    
    echo "Acumulado:".$acumulado. "</br>"; // Salida: Acumulado: 31
    
    //Usando la función array_filter obtener solo los números impares de los valores de $array
    $numeros_impares = array_filter($array, function ($elemento) {
        return $elemento % 2 !== 0; //Usamos mod, resto de división es diferente a cero cuando es impar
    });
    
    echo "Números impares: " . implode(", ", $numeros_impares) . "</br>"; //implode para mostrar arreglo como cadena de caracteres

    //Ordenar de forma inversa los valores de $array
    rsort($array);
    echo "Orden inverso: " . implode(", ", $array) . "</br>";
?>