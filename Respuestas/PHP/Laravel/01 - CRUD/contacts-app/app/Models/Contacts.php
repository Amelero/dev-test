<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    class Contacts extends Model
    {
        protected $table = 'contacts';
        protected $fillable = ['name', 'email', 'phone', 'age', 'active'];
        protected $guarded = ['id', 'created_at', 'updated_at'];
        use HasFactory;
    }
