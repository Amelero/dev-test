<?php   
    $cadena = '/Respuestas/PHP/DataManipulation/02/';

    // Conteo de caracteres
    echo "Conteo de caracteres: " . strlen($cadena) . "</br>";

    // Conteo de letras con exp. regular
    $letras = preg_replace('/[^a-zA-Z]/', '', $cadena); //buscamos solo letras (a-z A-Z) 
    echo "Conteo de letras: " . strlen($letras) . "</br>";
    
    // Conteo de letras en mayúscula
    $mayus = 0;
    for ($i = 0; $i < strlen($cadena); $i++) {
        if (ctype_upper($cadena[$i])) { //usando funcion para determinar si caract. es mayúscula o minúscula
            $mayus++;
        }
    }
    echo "Conteo de letras en mayúscula: " . $mayus . "</br>";

    // Reemplamos las cadenas a formato snake_case
    $snake_case = preg_replace('/[^a-zA-Z0-9]/', '_', strtolower($cadena));
    //resultado:  _respuestas_php_datamanipulation_02_ 
    //Ahora, para cumplir formato snake_case, eliminamos primer y último guión.

    $primer_caracter = substr($snake_case, 1); // Extraer la cadena sin el primer guion bajo  
    $longitud = strlen($primer_caracter) - 1; // Calcular la longitud de la cadena sin el primer guion bajo 
    $cadena_final = substr($primer_caracter, 0, $longitud); // Extraer la cadena sin el último guion bajo

    echo "Cadena en formato snake_case: " . $cadena_final . "</br>";
?>