<?php
    // $num = 1324223.1365593

    $num = 1324223.1365593;

    // Parte entera
    $parte_entera = intval($num);

    // Parte decimal
    $parte_decimal = $num - $parte_entera;

    // Redondeo a dos decimales
    $redondeo_dos_decimales = round($num, 2);

    // Redondeo Superior
    $redondeo_superior = ceil($num);

    // Redondeo Inferior
    $redondeo_inferior = floor($num);

    // Numero con formato (dos decimales) ##.###,##
    $numero_formateado = number_format($num, 2, ',', '.');

    // Resultados
    echo "Numero: ". $num .  "</br>";
    echo "Parte entera: " . $parte_entera . "</br>";
    echo "Parte decimal: " . $parte_decimal . "</br>";
    echo "Redondeo a dos decimales: " . $redondeo_dos_decimales . "</br>";
    echo "Redondeo Superior: " . $redondeo_superior . "</br>";
    echo "Redondeo Inferior: " . $redondeo_inferior . "</br>";
    echo "Numero con formato: " . $numero_formateado . "</br>";

?>