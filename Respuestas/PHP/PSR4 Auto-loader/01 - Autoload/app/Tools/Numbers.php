<?php

namespace App\Tools;

class Numbers
{
    public static function format($number)
    {
        return number_format($number, 2);
    }

    public static function money($number)
    {
        return '$ ' . self::format($number);
    }
}

?>