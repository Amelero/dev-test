// Ubicarse en directorio C:\xampp\htdocs\Respuestas\PHP\Composer\01 - Inicio> 
// Ejecutar composer init
// Resultado de la ejecución
{
    "name": "alvaro/respuestas",
    "description": "Creacion de composer",
    "type": "project",
    "require": {
        "nesbot/carbon": "3.x-dev"
    },
    "license": "MIT",
    "autoload": {
        "psr-4": {
            "Alvaro\\Respuestas\\": "src/"
        }
    },
    "authors": [
        {
            "name": "Alvaro",
            "email": "a20170284@pucp.edu.pe"
        }
    ],
    "minimum-stability": "dev"
}

// Observar que se creó la carpeta "Vendor", composer.json y composer.lock