<?php return array(
    'root' => array(
        'name' => 'alvaro/respuestas',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'alvaro/respuestas' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nesbot/carbon' => array(
            'pretty_version' => '3.x-dev',
            'version' => '3.9999999.9999999.9999999-dev',
            'reference' => 'de81149eec7f1949ef3cc1aeda24062f028198f3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nesbot/carbon',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'f9c7affe77a00ae32ca127ca6833d034e6d33f25',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(
                0 => '1.28.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/translation' => array(
            'pretty_version' => '6.3.x-dev',
            'version' => '6.3.9999999.9999999-dev',
            'reference' => 'a0e09f634b70f5a047bcd9c21b850b0f9d2e992c',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/translation-contracts' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '68cce71402305a015f8c1589bfada1280dc64fe7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/translation-contracts',
            'aliases' => array(
                0 => '3.3.x-dev',
            ),
            'dev_requirement' => false,
        ),
        'symfony/translation-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.3|3.0',
            ),
        ),
    ),
);
