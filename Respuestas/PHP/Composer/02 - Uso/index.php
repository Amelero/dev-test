<?php
    require '../../../vendor/autoload.php';

    use Carbon\Carbon;

    // Recuperar la fecha actual
    $fechaActual = Carbon::now();
    echo "Fecha actual: " . $fechaActual->format('d/m/Y') . "<br>";

    // Agregar una semana a la fecha
    $fechaActual->addWeek();
    echo "Fecha actual + 1 semana: " . $fechaActual->format('m/d/Y') . "<br>";

    // Restar 3 meses a la fecha
    $fechaActual = Carbon::now();
    $fechaActual->subMonths(3);
    echo "Fecha actual - 3 meses: " . $fechaActual->format('d/m/Y') . "<br>";
    
    
    // Presentar la fecha en formato Mes/Dia/Año
    $fechaActual = Carbon::now();
    echo "Fecha actual en formato Mes/Dia/Año: " . $fechaActual->format('m/d/Y') . "<br>";

    
    // Calcular el número de días entre tu día de nacimiento y la fecha actual
    $fechaActual = Carbon::now();
    $fechaNacimiento = Carbon::createFromDate(2000,06,26);
    $numeroDias = $fechaNacimiento->diffInDays($fechaActual);
    echo "Número de días entre mi día de nacimiento y la fecha actual: " . number_format($numeroDias,2, ".", ",") . "<br>";

?>
