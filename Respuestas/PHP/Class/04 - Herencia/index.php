<?php 
    require_once '../03 - Constantes/index.php';

    class Operations extends Numbers
    {
        public static function multi($num1, $num2)
        {
            return $num1 * $num2;
        }
    
        public static function div($num1, $num2)
        {
            return $num1 / $num2;
        }
    }

    // Validando
    echo "<br> Operaciones desde: 04 - Herencia/index.php </br>";
    echo Operations::sum(3, 5) . "<br>"; // 3 + 5 = 8
    echo Operations::diff(2, 4) . "<br>"; // 2 - 4 = -2
    echo Operations::multi(2, 4) . "<br>"; // 2 x 4 = 8
    echo Operations::div(6, 2) . "<br>"; // 6 / 2 = 3

    echo Operations::format(1262.25361154, 4) . "<br>"; // 1,262.2536
    echo Operations::format(1262.25361154) . "<br>"; // 1,262.25

    echo Operations::money(1262.25361154) . "<br>"; // $ 1,262.25

    // decimals = 4
    echo Operations::format(1262.25361154) . "<br>"; // 1,262.2536

    // decimals = 4
    // decimal separator = "."
    // thousands separator = " "
    echo Operations::format(1262.25361154) . "<br>"; // 1 262,2536
    echo Operations::money(1262.25361154) . "<br>"; // $ 1 262,2536
?>