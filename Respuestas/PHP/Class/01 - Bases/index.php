<?php  
    // Declaración de la clase Numbers
    class Numbers {
        public function sum($a, $b) { //Suma
            return $a + $b;
        }
        
        public function diff($a, $b) { //Resta
            return $a - $b;
        }
        
        public function format($num, $decimales = 2) { //decimals = 2 porque debe tener como mínimo 2 decimales
            return number_format($num, $decimales, ".", ",");
        }
        
        public function money($num) {
            return "$ " . $this->format($num, 2);
        }
    }

    // Validación
    $num = new Numbers();

    echo $num->sum( 3, 5)."</br>"; // 3 + 5 = 8
    echo $num->diff( 2, 4)."</br>"; // 2 - 4 = -2
    echo $num->format( 1262.25361154, 4)."</br>"; // 1,262.2536
    echo $num->format( 1262.25361154)."</br>"; // 1,262.25
    echo $num->money( 1262.25361154)."</br>"; // $ 1,262.25


?>