<?php  
    // Declaración de la clase Numbers. Agregamos "static"
    class Numbers {
        
        public static function sum($a, $b) { //Suma
            return $a + $b;
        }
        
        public static function diff($a, $b) { //Resta
            return $a - $b;
        }
        
        public static function format($num, $decimales = 2) { //decimals = 2 porque debe tener como mínimo 2 decimales
            return number_format($num, $decimales, ".", ",");
        }
        
        public static function money($num) {
            return "$ " . self::format($num);
        }
        
    }

    // Validación
    echo Numbers::sum(3, 5)."</br>"; // 8 
    echo Numbers::diff(2, 4)."</br>"; // -2   
    echo Numbers::format(1262.25361154, 4)."</br>"; // 1,262.2536
    echo Numbers::format(1262.25361154)."</br>"; // 1,262.25   
    echo Numbers::money(1262.25361154)."</br>"; // $ 1,262.25

?>