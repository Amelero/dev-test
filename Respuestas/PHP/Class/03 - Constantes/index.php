<?php  
    // Definimos clase NumbersConfig
    class NumbersConfig {
        const decimals = 4;
        const decimal_separator = ".";
        const thousands_separator = " ";
    
        public static function getDecimals() {
            return self::decimals;
        }
    
        public static function getDecimalSeparator() {
            return self::decimal_separator;
        }
    
        public static function getThousandsSeparator() {
            return self::thousands_separator;
        }
    }
    // Definimos clase Numbers
    class Numbers {
        public static function sum($a, $b) {
            return $a + $b;
        }
    
        public static function diff($a, $b) {
            return $a - $b;
        }
    
        public static function format($num, $decimales = NumbersConfig::decimals) {
            return number_format($num, $decimales, NumbersConfig::decimal_separator, NumbersConfig::thousands_separator);
        }
    
        public static function money($num) {
            return '$ ' . self::format($num);
        }
    }

    // Validando
    // decimals = 4
    echo Numbers::format( 1262.25361154)."</br>"; // 1 262.2536

    // decimals = 4
    // decimal separator = "."
    // thousands separator = " "
    echo Numbers::format( 1262.25361154)."</br>"; // 1 262.2536
    echo Numbers::money( 1262.25361154)."</br>"; // $ 1 262.2536


?>